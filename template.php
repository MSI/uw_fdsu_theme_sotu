<?php
/**
 * @file
 * Template.php.
 */

/**
 * Implements hook_page_alter().
 */
function uw_fdsu_theme_sotu_page_alter(&$page) {
  // Force the global footer region to display when empty.
  if (!isset($page['global_footer'])) {
    foreach (system_region_list($GLOBALS['theme'], REGIONS_ALL) as $region => $name) {
      if (in_array($region, array('global_footer'))) {
        $page['global_footer'] = array(
          '#region' => 'global_footer',
          '#weight' => '-10',
          '#theme_wrappers' => array('region'),
        );
      }
    }
  }
}

/**
 * Implements hook_preprocess_html().
 *
 * Add custom color css file.
 */
function uw_fdsu_theme_sotu_preprocess_html(&$variables) {
  // Add to body element a class to express
  // the faculty or other organizational unit that this page is from.
  // This is used to select the faculty colors in CSS.
  $variables['classes_array'][] = 'org_' . variable_get('uw_fdsu_theme_color_css', 'default');

  // Variable to Add a loading class to body if homepage.
  if (drupal_is_front_page()) {
    $variables['classes_array'][] = 'loading';
  }
  // Variable to add the uw-sotu to the body.
  $variables['classes_array'][] = 'uw-sotu';

  // Add javascript for logged in users.
  global $user;
  if ($user->uid) {
    drupal_add_js(drupal_get_path('theme', 'uw_fdsu_theme') . '/scripts/hide-admin.js', 'file');
  }
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge',
    ),
  );
  $meta_mobile_view = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1',
    ),
  );
  $sotu_path = drupal_get_path('theme', 'uw_fdsu_theme_sotu') . '/css/fdsu_sotu.css';
  drupal_add_css($sotu_path);
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_mobile_view, 'meta_mobile_view');
}

/**
 * Implements hook_css_alter().
 */
function uw_fdsu_theme_sotu_css_alter(&$css) {
  // uw_core_theme and uw_fdsu_theme are
  // loaded, but we don't want their CSS.
  // Remove any CSS added by these.
  $path = drupal_get_path('theme', 'uw_core_theme') . '/';
  foreach ($css as $key => $source) {
    if (substr($key, 0, strlen($path)) == $path) {
      unset($css[$key]);
    }
  }
  // Remove CSS added by various modules.
  $path_calendar = drupal_get_path('module', 'calendar') . '/css/';
  $path_uw_ct_bibliography = drupal_get_path('module', 'uw_ct_bibliography') . '/css/';
  $path_uw_ct_custom_listing = drupal_get_path('module', 'uw_ct_custom_listing') . '/css/';
  $path_uw_ct_embedded_timeline = drupal_get_path('module', 'uw_ct_embedded_timeline') . '/css/';
  $path_uw_ct_special_alert = drupal_get_path('module', 'uw_ct_special_alert') . '/css/';
  $path_uw_ct_image_gallery = drupal_get_path('module', 'uw_ct_image_gallery') . '/css/';
  $path_uw_ct_project = drupal_get_path('module', 'uw_ct_project') . '/css/';
  $path_uw_ct_promo_item = drupal_get_path('module', 'uw_ct_promo_item') . '/css/';
  $path_aggregator = drupal_get_path('module', 'aggregator') . '/';
  $path_colorbox = drupal_get_path('module', 'colorbox') . '/styles/default/';
  $path_ctools = drupal_get_path('module', 'ctools') . '/css/';
  $path_jquery_update = drupal_get_path('module', 'jquery_update') . '/replace/ui/themes/base/minified/';
  $path_jquery_update = drupal_get_path('module', 'jquery_update') . '/replace/ui/themes/base/minified/';
  $path_addtoany = drupal_get_path('module', 'addtoany') .  '/';
  $path_ckeditor_socialmedia = drupal_get_path('module', 'ckeditor_socialmedia') .  '/css/';
  $path_uw_a11y = drupal_get_path('module', 'uw_a11y') . '/css/';
  $path_ctools = drupal_get_path('module', 'ctools') . '/css/';
  $path_uw_nav_global_footer = drupal_get_path('module', 'uw_nav_global_footer') . '/css/';
  $path_uw_nav_site_footer = drupal_get_path('module', 'uw_nav_site_footer') . '/css/';
  $path_system = drupal_get_path('module', 'system') . '/';
  $path_system = drupal_get_path('module', 'system') . '/';
  $path_system = drupal_get_path('module', 'system') . '/';
  $path_uw_ct_embedded_facts_and_figures = drupal_get_path('module', 'uw_ct_embedded_facts_and_figures') . '/css/';
  $path_uw_ct_embedded_call_to_action = drupal_get_path('module', 'uw_ct_embedded_call_to_action') .  '/css/';
  $path_uw_ct_home_page_banner = drupal_get_path('module', 'uw_ct_home_page_banner') . '/css/';
  $path_uw_ct_home_page_banner = drupal_get_path('module', 'uw_ct_home_page_banner') . '/css/';
  $path_uw_ct_service = drupal_get_path('module', 'uw_ct_service') . '/css/';
  $path_uw_ct_blog = drupal_get_path('module', 'uw_ct_blog') . '/css/';
  $path_uw_ct_news_item = drupal_get_path('module', 'uw_ct_news_item') . '/css/';
  $path_uw_ct_event = drupal_get_path('module', 'uw_ct_event') . '/css/';
  $path_uw_ct_opportunities = drupal_get_path('module', 'uw_ct_opportunities') . '/css/';
  $path_uw_ct_person_profile = drupal_get_path('module', 'uw_ct_person_profile') . '/css/';
  $path_uw_social_media_sharing = drupal_get_path('module', 'uw_social_media_sharing') .  '/css/';
  $path_date = drupal_get_path('module', 'date') .  '/date_api/';
  $path_date = drupal_get_path('module', 'date') . '/date_views/css/';
  $path_uw_ct_contact = drupal_get_path('module', 'uw_ct_contact') .  '/css/';
  $path_views = drupal_get_path('module', 'views') . '/css/';
  $exclude = array(
    $path_calendar .   'calendar_multiday.css' => TRUE,
    $path_uw_ct_bibliography .   'uw_ct_bibliography.css' => TRUE,
    $path_uw_ct_custom_listing . 'uw_ct_custom_listing.css' => TRUE,
    $path_uw_ct_special_alert . 'uw_ct_embedded_timeline.css' => TRUE,
    $path_uw_ct_special_alert . 'uw_ct_special_alert.css' => TRUE,
    $path_uw_ct_image_gallery . 'image-galleries.css' => TRUE,
    $path_uw_ct_project . 'uw_ct_project.css' => TRUE,
    $path_uw_ct_promo_item . 'uw_ct_promo_item.css' => TRUE,
    $path_aggregator . 'aggregator.css' => TRUE,
    $path_colorbox . 'colorbox_style.css' => TRUE,
    $path_ctools . 'ctools.css' => TRUE,
    $path_jquery_update . 'jquery.ui.core.min.css' => TRUE,
    $path_jquery_update . 'jquery.ui.theme.min.css ' => TRUE,
    $path_addtoany  . 'addtoany.css' => TRUE,
    $path_ckeditor_socialmedia . 'ckeditor_socialmedia.css' => TRUE,
    $path_uw_a11y . 'uw_a11y.css' => TRUE,
    $path_ctools . 'modal.css' => TRUE,
    $path_uw_nav_global_footer . 'uw_nav_global_footer.css' => TRUE,
    $path_uw_nav_site_footer . 'uw_nav_site_footer.css' => TRUE,
    $path_system . 'system.messages.css' => TRUE,
    $path_system . 'system.base.css' => TRUE,
    $path_system . 'system.theme.css' => TRUE,
    $path_system . 'system.menue.css' => TRUE,
    $path_uw_ct_embedded_facts_and_figures . 'highlighted_fact.css' => TRUE,
    $path_uw_ct_embedded_facts_and_figures . 'uw_ct_embedded_facts_and_figures.css' => TRUE,
    $path_uw_ct_embedded_call_to_action . 'uw_ct_embedded_call_to_action.css' => TRUE,
    $path_uw_ct_home_page_banner . 'banner.css' => TRUE,
    $path_uw_ct_home_page_banner . 'uw_banner_slideshow.css' => TRUE,
    $path_uw_ct_service  . 'uw_ct_service.css' => TRUE,
    $path_uw_ct_blog . 'uw_ct_blog.css' => TRUE,
    $path_uw_ct_news_item . 'uw_ct_news_item.css' => TRUE,
    $path_uw_ct_event  . 'uw_ct_event.css' => TRUE,
    $path_uw_ct_opportunities  . 'uw_ct_opportunities.css' => TRUE,
    $path_uw_ct_person_profile . 'uw_ct_person_profile.css' => TRUE,
    $path_uw_social_media_sharing . 'uw_social_media_sharing.css' => TRUE,
    $path_date . 'date.css' => TRUE,
    $path_date . 'date_views.css' => TRUE,
    $path_uw_ct_contact . 'uw_ct_contact.css' => TRUE,
    $path_views . 'views.css' => TRUE,

  );
  $css = array_diff_key($css, $exclude);
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param string $variables
 *   An array containing the breadcrumb links.
 *
 * @return string
 *   A string containing the breadcrumb output.
 */
function uw_fdsu_theme_sotu_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  // Remove the local site home link,
  // and replace it with a link that uses the site name.
  if (isset($breadcrumb[0])) {
    array_shift($breadcrumb);
    array_unshift($breadcrumb, l(variable_get('site_name', 'Site home'), '<front>'));
  }

  // Return the breadcrumb with separators.
  if (!empty($breadcrumb)) {
    $separator = ' » ';

    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $heading = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    return $heading . '<div class="breadcrumb">' . implode($separator, $breadcrumb) . $separator . '</div>';
  }
}

/**
 * Add theme suggestion for all content types.
 */
function uw_fdsu_theme_sotu_process_page(&$variables) {

  // Use the asterix wrapped words in title
  // field to allow design direct the homepage.
  if (isset($site_name)) {
    $site_name = variable_get('site_name');
    $pattern = '\*(.+?)\*';
    $replacement = "<span>$1</span>";
    $site_name = variable_set('site_name', preg_replace("/\*(.+?)\*/", "<span>$1</span>", $site_name));

  }

  if (isset($variables['node'])) {
    if ($variables['node']->type != '') {
      $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
    }
  }
}

/**
 * Find related articles by taxonomy.
 */
function uw_fdsu_theme_sotu_find_by_taxonomy($taxonomies, $skipnodes, $limit) {
  $nodes = array();
  // If no taxonomies are set, don't return anything.
  if (count($taxonomies)) {
    // Article must be the uWaterloo Stories content type.
    $where[] = "n.type = 'uw_stories'";
    // Article must be published.
    $where[] = 'n.status = 1';
    // Article must belong to at least one specified taxonomy.
    $where[] = 'tn.tid IN (' . implode(',', $taxonomies) . ')';
    // Article must not be one of the previously specified articles.
    $where[] = 'n.nid NOT IN (' . implode(',', $skipnodes) . ')';
    // Build query.
    $sql = 'SELECT n.nid, n.title, COUNT(tn.tid) as count
        FROM {node} n
        INNER JOIN {taxonomy_index} tn USING (nid)
        WHERE ' . implode(' AND ', $where) . '
        GROUP BY n.nid
        ORDER BY count DESC, n.created DESC
        LIMIT ' . $limit;
    // Execute query.
    $result = db_query($sql);
    // Loop through results.
    foreach ($result as $node) {
      $nodes[] = $node;
    }
  }
  return $nodes;
}

/**
 * Implements hook_js_alter().
 */
function uw_fdsu_theme_sotu_js_alter(&$javascript) {
  global $base_path;
  global $base_url;
  if (module_exists('uw_header_search')) {
    $rwdjs_path = drupal_get_path('module', 'uw_header_search') . '/uw_header_search.js';
    drupal_add_js($rwdjs_path);
    // drupal_add_js($tabsjs_path);
    drupal_add_js(array(
      'CToolsModal' => array(
        'modalSize' => array(
          'type' => 'scale',
          'width' => 1,
          'height' => 1,
        ),
        'modalOptions' => array(
          'opacity' => .98,
          'background-color' => '#252525',
        ),
        'animation' => 'fadeIn',
        'animationSpeed' => 'slow',
        'throbberTheme' => 'CToolsThrobber',
        // Customize the AJAX throbber like so:
        // This function assumes the images
        // are inside the module directory's "images"
        // directory:
        'throbber' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t('Loading...'),
          'title' => t('Loading'),
        )),
        'closeText' => '',
        'closeImage' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t(''),
          'title' => t(''),
        )),
      ),
    ), 'setting');
  }
  $exclude = array();
  $javascript = array_diff_key($javascript, $exclude);
}
