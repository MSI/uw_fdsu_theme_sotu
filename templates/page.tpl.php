<?php

/**
 * @file
 * Template for almost the entire page. Most of the page is inside div#site.
 */
  global  $base_url;
  $path = $_SERVER['REQUEST_URI'];
  $find = 'archive';
  $pos = strpos($path, $find);
  $uw_theme_branding = variable_get('uw_theme_branding', 'full');

  $uw_sotu_theme_path = drupal_get_path('theme', 'uw_fdsu_theme_sotu') . '/';
  $uw_sotu_themes_img_path = ($base_url . '/' . $uw_sotu_theme_path . 'images/');
  $term_page = menu_get_object('taxonomy_term', 2);
  $find_2 = 'presidents-message';
  $pos_2 = strpos($path, $find_2);
?>

<?php if ($pos !== FALSE || $pos_2 !== FALSE): ?>
    <?php $pos = "archive";?>
    <?php $pos_2 = "presidents-message";?>
<?php endif; ?>
<div class="breakpoint"><div id="speed"></div></div>
<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site uw-site--sotu <?php print $pos; ?>"<?php print $attributes; ?>>
  <div class="uw-site--inner">
    <!--Section Skiplinks -->
    <div id="skip-links" class="uw-site--skip">
      <div class="uw-section--inner">
        <div id="skip" class="skip">
          <a href="#main" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
          <a href="#footer" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
        </div>
      </div>
    </div>
    <!--Section Header -->
    <div id="header" class="uw-header--global">
        <div class="uw-section--inner">
            <?php print render($page['global_header']); ?>
      </div>
    </div>
    <div id="site--offcanvas" class="uw-site--off-canvas <?php print ($uw_theme_branding === 'full') ? 'non_generic_header' : 'generic_header'; ?>">
      <div class="uw-section--inner">
        <?php print render($page['site_header']); ?>
      </div>
    </div>
    <!--Section Color bar -->
    <?php if ($uw_theme_branding === 'full'): ?>
    <div id="site-colors" class="uw-site--colors">
       <div class="uw-site--cbar">
         <div class="uw-site--c1 uw-cbar"></div>
         <div class="uw-site--c2 uw-cbar"></div>
         <div class="uw-site--c3 uw-cbar"></div>
         <div class="uw-site--c4 uw-cbar"></div>
       </div>
     </div>
    <?php endif; ?>
    <div id="site-header" class="uw-site--header">
      <?php if (!drupal_is_front_page()) :?>
          <a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home">
            <?php print $site_name; ?>
          </a>
        <?php endif; ?>
    </div>
    <div id="uw-stories--main" class="uw-stories--main">
          <div class="uw-section--inner">
            <?php if (isset($site_slogan) && $site_slogan !== "") : ?>
              <span class="uw-site—subtitle"> <?php print $site_slogan; ?> </span>
            <?php endif; ?>
            <?php if (drupal_is_front_page()) :?>
            <div id="box" class="box">
              <div class="uw-site-—title">
                <h1><?php print $site_name; ?></h1>
              </div>
              <div id="uw-box-logo" class="uw-site--logo">
                <a id="uw-site-logo" class="uw-site--boxlogo" href="https://uwaterloo.ca/">University of Waterloo</a>
              </div>
              <?php if (isset($site_slogan) && $site_slogan !== "") : ?>
              <div class="uw-site—subtitle">
                <span><?php print $site_slogan; ?></span>
              </div>
              <?php endif; ?>
                <div class="layer" id="l1">
                   <div class="layer-l1">&nbsp;</div>
                 </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div id="content-banner" class="uw-site--featured-stories uw_stories_sticky">
        <div class="uw-section--inner">
            <?php print render($page['banner']); ?>
        </div>
    </div>

    <div id="main" class="uw-site--main role="main" <?php print $pos_2; ?>">
      <div class="uw-section--inner">
        <div class="uw-site--main-top">
          <div class="uw-site--messages">
              <?php print $messages; ?>
          </div>
          <div class="uw-site--help">
              <?php print render($page['help']); ?>
          </div>
          <div class="uw-site--breadcrumb">
          <!--  <//?php  print $breadcrumb; ?> -->
          </div>
          <div class="uw-site--title">
            <?php if (!drupal_is_front_page()) :?>
              <h1>
              <?php if ($title != "Global Impact") :?>
                <?php print $title ? $title : $site_name;?>
                <?php else: ?>
                <?php print '<div id="uw-story">' . $title . '</div>';?>
                <?php endif; ?>
              </h1>
            <?php endif; ?>
          </div>
          <!-- when logged in -->
          <?php if ($tabs): ?>
            <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?></div>
            <?php
          endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
        </div>
        <?php $sidebar = render($page['sidebar_second']); ?>
        <?php $sidebar_promo = render($page['promo']); ?>
        <div class="uw-site-main--content" <?php  if(isset($sidebar) || isset($sidebar_promo) || isset($content['feature_field_sidebar'])) : ?><?php if(($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '')): ?><?php echo 'sidebar-present';?><?php else:?><?php echo 'no-sidebar-present';?><?php endif; ?><?php endif; ?>">
          <div id="content" class="uw-site-content uw_stories_non_sticky">
              <?php if(isset($show_layout) && $show_layout && $term_page) :?>
                <input type="checkbox" id="article-layout-dir" class="layout-dir-check"/>
                <div class="article-list-layout">
                  <label for="article-layout-dir" class="article-layout-button">
                    <span class="article-layout-list"><span class="ifdsu fdsu-list-view"></span> List </span>
                    <span class="article-layout-grid"><span class="ifdsu fdsu-grid-view" ></span> Grid </span>
                  </label>
                </div>
              <?php endif; ?>
              <?php print render($page['content']);?>


          </div><!--/main-content-->
          <?php if(isset($sidebar) || isset($sidebar_promo) || isset($content['feature_field_sidebar'])) : ?>
              <?php if(($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '')) : ?>
                  <div id="site-sidebar-wrapper" class="uw-site-sidebar--wrapper uw-sotu-sidebar">
                      <div id="site-sidebar-nav" class="uw-site-sidebar--nav">
                          <div id="site-sidebar" class="uw-site-sidebar <?php if($sidebar_promo !== NULL && $sidebar_promo !== '') :?><?php echo ('sticky-promo');?><?php endif; ?>">
                              <?php if(isset($sidebar_promo)) : ?>
                                  <?php if($sidebar_promo !== NULL && $sidebar_promo !== ''): ?>
                                      <div class="uw-site-sidebar--promo">
                                          <?php print render($page['promo']); ?>
                                      </div>
                                  <?php endif; ?>
                              <?php endif; ?>
                              <div class="uw-site-sidebar--second">
                                  <?php print render($page['sidebar_second']); ?>

                              </div>
                              <?php if(isset($content['feature_field_sidebar'])) : ?>
                                  <?php if($content['feature_field_sidebar'] !== NULL && $content['feature_field_sidebar'] !== ''): ?>
                                      <div class="uw-site-sidebar--comp">
                                          <?php print ($content['feature_field_sidebar']); ?>
                                      </div>
                                  <?php endif; ?>
                              <?php endif; ?>
                          </div>
                      </div>
                  </div>
              <?php endif; ?>
          <?php endif; ?>
        </div>
      </div>
      <!--/section inner-->
    </div>
    <?php if (drupal_is_front_page()) :?>
       <div id="sotu-themes2" class="sotu_themes">
        <section class="sotu_theme__content theme_3">
               <div class="sotu_theme__content_wrap">
                   <h2 class="sotu_theme__content_title">Human&ndash;Machine Interaction</h2>
                   <div class="sotu_theme__content_sub">
                       <p>
                           <span>As the line between human and machine is</span>
                           <span>increasingly blurry, how should we build and use</span>
                           <span>technology to advance humanity?</span>
                       </p>
                   </div>
                   <div class="sotu_theme__content__p">
                       <p>
                           Technology is augmenting every aspect of our lives and we are relying on machines to do everything from drive our cars to diagnose illness. If we are to responsibly develop intelligent robotics and systems, we must recognize both the limits of machines and the needs of people and our complex society.               </p>
                       <div class="call-to-action-center-wrapper">
                           <aside><a href="https://uwaterloo.ca/stories/global-impact/human-machine-interaction">
                                   <div class="call-to-action-wrapper">
                                       <div class="call-to-action-theme-uWaterloo">
                                           <div class="call-to-action-small-text">Find out more about:</div>
                                           <div class="call-to-action-big-text">HUMAN&ndash;MACHINE INTERACTION</div>
                                       </div>
                                   </div>
                               </a>
                           </aside>
                       </div>
                   </div>
               </div>
               <div class="sotu_theme__image_wrap">
                   <div class="sotu_theme__image-bg"></div>
                   <img class="sotu_theme__image" src="<?php echo $uw_sotu_themes_img_path; ?>hmi-2.jpg" alt="Human-Machine Interaction"/>
               </div>
           </section>
        <section class="sotu_theme__content theme_2">
               <div class="sotu_theme__content_wrap">
                   <h2 class="sotu_theme__content_title">Next&ndash;Generation Computing</h2>
                   <div class="sotu_theme__content_sub">
                       <p>  <span>How do we securely leverage promising  </span>
                           <span>new computing technologies and big data</span>
                           <span>to solve global problems?</span>
                   </div></p>
                   <div class="sotu_theme__content__p">
                       <p>
                           While innovators are finding new ways to use big data to improve our lives, others are in a race to protect sensitive data in a future where quantum devices, information and technologies bring both promise and risk.               </p>
                       <div class="call-to-action-center-wrapper">
                           <aside><a href="https://uwaterloo.ca/stories/global-impact/next-generation-computing">
                                   <div class="call-to-action-wrapper">
                                       <div class="call-to-action-theme-uWaterloo">
                                           <div class="call-to-action-small-text">Find out more about:</div>
                                           <div class="call-to-action-big-text">NEXT&ndash;GENERATION COMPUTING</div>
                                       </div>
                                   </div>
                               </a>
                           </aside>
                       </div>
                   </div>
               </div>
               <div class="sotu_theme__image_wrap">
                   <div class="sotu_theme__image-bg"></div>
                   <img class="sotu_theme__image" src="<?php echo $uw_sotu_themes_img_path; ?>ngc.jpg" alt="Next-Generation Computing"/>
               </div>
           </section>
        <section class="sotu_theme__content theme_1">
          <div class="sotu_theme__content_wrap">
            <h2 class="sotu_theme__content_title">Social and Economic Prosperity</h2>
            <div class="sotu_theme__content_sub">
              <p>
              <span>As our world becomes increasingly connected,</span>
              <span>how do we ensure an equitable transformation that</span>
              <span>builds community and prosperity for all? </span></p>
            </div>
            <div class="sotu_theme__content__p">
            <p>
                We are at the cusp of the fourth industrial revolution: the digital revolution. The growing adoption of artificial intelligence and technologies is changing our workforce and economy. At the same time, migration is at a record high due to natural and political disruption. So, while millions search for their next home, others are looking for their next job.
            </p>
            <div class="call-to-action-center-wrapper">
              <aside><a href="https://uwaterloo.ca/stories/global-impact/social-and-economic-prosperity">
                <div class="call-to-action-wrapper">
                  <div class="call-to-action-theme-uWaterloo">
                    <div class="call-to-action-small-text">Find out more about:</div>
                    <div class="call-to-action-big-text">SOCIAL AND ECONOMIC PROSPERITY</div>
                  </div>
                </div>
              </a>
            </aside>
            </div>
          </div>
          </div>
          <div class="sotu_theme__image_wrap">
            <div class="sotu_theme__image-bg"></div>
            <img class="sotu_theme__image" src="<?php echo $uw_sotu_themes_img_path; ?>ps.jpg" alt="Social and Economic Prosperity"/>
          </div>
        </section>
        <section class="sotu_theme__content theme_4">
          <div class="sotu_theme__content_wrap">
              <h2 class="sotu_theme__content_title">Sustainable Planet</h2>
              <div class="sotu_theme__content_sub">
                <p>  <span>How will we navigate the social, economic </span>
                <span> and geopolitical changes required to sustain </span>
                <span> our future on Earth?</span></p>
              </div>
              <div class="sotu_theme__content__p">
              <p>
                  Human activity has created a global climate crisis that requires collective and immediate action. As greenhouse gas emissions climb, many search for clean technologies and sustainable energy alternatives. Climate disruption is also intricately connected to our housing, food and water security.
              </p>
              <div class="call-to-action-center-wrapper">
                <aside><a href="https://uwaterloo.ca/stories/global-impact/sustainable-planet">
                  <div class="call-to-action-wrapper">
                    <div class="call-to-action-theme-uWaterloo">
                        <div class="call-to-action-small-text">Find out more about:</div>
                      <div class="call-to-action-big-text">SUSTAINABLE PLANET</div>
                    </div>
                  </div>
                </a>
              </aside>
              </div>
            </div>
          </div>
          <div class="sotu_theme__image_wrap">
            <div class="sotu_theme__image-bg"></div>
            <img class="sotu_theme__image" src="<?php echo $uw_sotu_themes_img_path; ?>Sustainable-planet-theme-image.jpg" alt="Sustainable Planet"/>
          </div>
        </section>
        <section class="sotu_theme__content theme_5">
          <div class="sotu_theme__content_wrap">
            <h2 class="sotu_theme__content_title">Technology and Human Health</h2>
            <div class="sotu_theme__content_sub">




              <p>  <span>How will new technologies and</span>
              <span>processes prevent disease and enable</span>
              <span>medical professionals to improve the</span>
                  <span>life-saving care they provide?</span></p>
            </div>
            <div class="sotu_theme__content__p">
            <p>
                Global and individual health care continues to be deeply enhanced by technology. In the race to make timely diagnoses and reduce wait times and costs, it&rsquo;s not just medical doctors who are saving lives: it is also engineers, practitioners, scientists and mathematicians.             </p>
            <div class="call-to-action-center-wrapper">
              <aside><a href="https://uwaterloo.ca/stories/global-impact/technology-human-health">
                <div class="call-to-action-wrapper">
                  <div class="call-to-action-theme-uWaterloo">
                      <div class="call-to-action-small-text">Find out more about:</div>
                    <div class="call-to-action-big-text">TECHNOLOGY AND HUMAN HEALTH</div>
                  </div>
                </div>
              </a>
            </aside>
            </div>
              </div>
          </div>
          <div class="sotu_theme__image_wrap">
              <div class="sotu_theme__image-bg"></div>
              <img class="sotu_theme__image" src="<?php echo $uw_sotu_themes_img_path; ?>ha.jpg" alt="Technology and Human Health"/>
          </div>
        </section>
        <section class="sotu_theme__content theme_6">
               <div class="sotu_theme__content_wrap">
                   <h2 class="sotu_theme__content_title">TRANSFORMATIONAL DISCOVERIES</h2>
                   <div class="sotu_theme__content_sub">
                       <p>  <span>How do we continue to encourage the curious</span>
                           <span>to further our understanding of how the world works?</span>
                   </div>
                   <div class="sotu_theme__content__p">
                       <p>
                           Transformation is borne out of curiosity. The next big breakthrough to rock society will be built on the shoulders of the science that came before it — discoveries that result from an open and curious mind probing the mysteries of the tiniest particles on earth or the vast universe.</p>
                       <div class="call-to-action-center-wrapper">
                           <aside><a href="https://uwaterloo.ca/stories/global-impact/transformational-discoveries">
                                   <div class="call-to-action-wrapper">
                                       <div class="call-to-action-theme-uWaterloo">
                                           <div class="call-to-action-small-text">Find out more about:</div>
                                           <div class="call-to-action-big-text">TRANSFORMATIONAL DISCOVERIES</div>
                                       </div>
                                   </div>
                               </a>
                           </aside>
                       </div>
                   </div>
               </div>
               <div class="sotu_theme__image_wrap">
                   <div class="sotu_theme__image-bg"></div>
                   <img class="sotu_theme__image" src="<?php echo $uw_sotu_themes_img_path; ?>crnw.jpg" alt="TRANSFORMATIONAL DISCOVERIES"/>
               </div>
           </section>
      </div>
      <div id="sidebar-home" class="uw-site-sotu-sidebar">
        <div class="uw-section--inner">
          <?php print render($page['sidebar_second']); ?>
        </div>
      </div>
    <?php endif; ?>
    <!--/site main-->
    <?php if (!drupal_is_front_page()) :?>
      <?php if (isset($watermark_term)):?>
        <div id="tag" class="uw-site--tag">
            <div class="uw-section--inner">
              <div class="uw-site--tag-wrap">
                <span class="uw-site--tag-text"> <?php print $watermark_term;?></span>
                <div class="facts"></div>
              </div>
            </div>
        </div>
      <?php endif; ?>
    <?php endif; ?>
    <div id="footer" class="uw-footer">
      <div id="uw-site-share" class="uw-site-share">
        <div class="uw-section--inner">
          <div class="uw-section-share"></div>
          <ul class="uw-site-share-top">
            <li class="uw-site-share--button__top">
              <a href="#main" id="uw-top-button" class="uw-top-button">
                <span class="ifdsu fdsu-arrow"></span>
                <span class="uw-footer-top-word">TOP</span>
              </a>
            </li>
            <li class="uw-site-share--button__share">
              <a href="#uw-site-share" class="uw-footer-social-button">
                <span class="ifdsu fdsu-share"></span>
                <span class="uw-footer-share-word">Share</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div id="site-footer" class="uw-site-footer site-footer-toggle open-site-footer">
        <div class="uw-section--inner">
          <div class="uw-site-footer1<?php print empty($page['site_footer']) ? ' uw-no-site-footer' : ''; ?>">
            <div class="uw-site-footer1--logo-dept">
              <?php
              $site_logo = variable_get('uw_nav_site_footer_logo');
              $site_logo_link = variable_get('uw_nav_site_footer_logo_link');
              $facebook = variable_get('uw_nav_site_footer_facebook');
              $twitter = variable_get('uw_nav_site_footer_twitter');
              $instagram = variable_get('uw_nav_site_footer_instagram');
              $youtube = variable_get('uw_nav_site_footer_youtube');
              $linkedin = variable_get('uw_nav_site_footer_linkedin');
              $snapchat = variable_get('uw_nav_site_footer_snapchat');

              $alt_tag = str_replace('_', ' ', $site_logo) . ' logo';
              $arr = explode(" ", $alt_tag);
              $arr = array_unique($arr);
              $alt_tag = implode(" ", $arr);
              ?>
              <?php if ($site_logo_link) : ?>
                <a href="<?php print $site_logo_link; ?>"><img src="<?php print base_path() . drupal_get_path('module', 'uw_nav_site_footer'); ?>/logos/<?php print $site_logo; ?>.png" alt="<?php print $alt_tag; ?>" /></a>
              <?php else : ?>
                <?php $site_name = (strtolower($site_name)); print '<a href="' . url('<front>') . '">' . (ucfirst($site_name)) . '</a>'; ?>
              <?php endif; ?>
            </div>
            <div class="uw-site-footer1--contact">
              <ul class="uw-footer-social">
                <?php if ((string) $facebook !== ''): ?>
                  <li>
                    <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>" aria-label="facebook">
                      <span class="ifdsu fdsu-facebook"></span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ((string) $twitter !== ''): ?>
                  <li>
                    <a href="https://www.twitter.com/<?php print check_plain($twitter); ?>" aria-label="twitter">
                      <span class="ifdsu fdsu-twitter"></span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ((string) $youtube !== ''): ?>
                  <li>
                    <a href="<?php print check_plain($youtube); ?>" aria-label="youtube">
                      <span class="ifdsu fdsu-youtube"></span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ((string) $instagram !== ''): ?>
                  <li>
                    <a href="<?php print check_plain($instagram); ?>" aria-label="instagram">
                      <span class="ifdsu fdsu-instagram"></span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ((string) $linkedin !== ''): ?>
                  <li>
                    <a href="<?php check_plain(print $linkedin); ?>" aria-label="linkedin">
                      <span class="ifdsu fdsu-linkedin"></span>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </div>
          </div>
          <?php if (!empty($page['site_footer']['uw_nav_site_footer_site-footer']['#node']->field_body_no_summary)): ?>
            <div class="uw-site-footer2">
              <?php print render($page['site_footer']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
      <?php print render($page['global_footer']); ?>
     </div>
  </div>

</div><!--/site-->
<div class="ie-resize-fix"></div>
