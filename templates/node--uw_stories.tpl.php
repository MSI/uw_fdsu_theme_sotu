<?php

/**
 * @file
 * Template for the node.
 */

?>
<?php if (!empty($content['links'])): ?>
<div class="links addtoany-wrap"><?php hide($content['links']['#links']['node-readmore']); print render($content['links']); ?>

</div>
<?php endif; ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner clearfix">
    <div class="uw_ct_story">
      <!-- If 'Page'-->
      <?php if (!$page):?>
        <a href="<?php print $node_url; ?>">
        <!-- If 'image is there  Print out the the image -->
        <?php if($content['feature_image']['uri'] !== NULL): ?>
          <div class="stories_featured_image">
            <picture class="stories_featured_img">
              <img src="<?php print $content['feature_image_style']['large']; ?>" alt="<?php print $content['feature_image']['alt']; ?>">
            </picture>
          </div>
          <!-- Else print out the holder-->
        <!-- Else use the default image for grid and list views -->
        <?php else: ?>
          <?php
            global $base_path;
            $theme_path = $base_path . drupal_get_path('theme', 'uw_fdsu_theme_sotu');
          ?>
          <div class="stories_featured_image">
            <img class="stories_featured_img" src="<?php print $theme_path; ?>/images/holder.png" alt="University of Waterloo">
          </div>
        <?php endif; ?>

          <div class="article-hp">
              <span class="article-date"><?php print $content['date_created']; ?></span>
              <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
              <?php if (isset($content['field_subhead']) && $content['field_subhead']): ?>
                <?php print render($content['field_subhead']); ?>
              <?php endif; ?>
          </div>
        </a>
      <?php else: ?>
      <?php if ($page): ?>


     <?php endif; ?>
      <div class="content_node"<?php print $content_attributes; ?>>
        <?php
          // We hide the comments, links and addtoany
          // now so that we can render them later.
          unset($content['field_photo']);
          hide($content['comments']);
          hide($content['links']);
          hide($content['addtoany']);

          // Hide links to other stories.
          unset($content['field_related_stories']);
          unset($content['field_link_uw_stories']);
        ?>
        <?php if(node_is_page($node)): ?>
          <?php print render($content['body']);?>

        <?php endif;?>
      </div>
        <?php if (!empty($content['field_topics_area']) || !empty($content['field_topics_societal_relevance']) || !empty($content['field_topics_societal_relevance'])): ?>
          <div class="stories-footer-topics">
            <h2>Tags</h2>
            <?php print render($content['field_topics_societal_relevance']); ?>
            <?php print render($content['field_topics_area']); ?>
            <?php print render($content['field_topics_strategic_alignment']); ?>

          </div>
        <?php endif;?>
        <div class="other-links">
          <?php if (isset($related_stories)): ?>
           <h2>Related stories</h2>
            <div class="related-stories">
              <ul class="stories-related-list">
              <?php foreach($related_stories as $key => $story): ?>
                <?php
                // This is the related stories that is
                // generated in the module file.
                  echo '<li class="stories-related-item " id="related-story-' . $key . '">';
                  echo '<a class="stories-related-link" href="' . $story['url'] . '">';
                  echo '<div class="stories-related-img">';
                  echo '<figure class="effect-lily">';
                  echo $story['image'];
                  echo '<figcaption><div class="stories-image-info"><p><span class="button">Read more</span></p></div></figcaption>';
                  echo '</figure>';
                  echo '</div>';
                  echo '<span class="stories-related-info">';
                  echo '<h3 class="stories-related-headline">' . $story['title'] . '</h3>';
                  echo $story['subhead'];
                  echo '</span>';
                  echo '</a>';
                  echo '</li>';
                ?>
              <?php endforeach;?>
              </ul>
            </div>
          <?php endif;?>
          <?php if (isset($link_uw_stories)): ?>
            <div class="external-links">
              <h2>External links</h2>
              <ul class="external-links-list">
              <?php foreach($link_uw_stories as $storylinks): ?>
                <?php
                // This is the linked stories that is
                // generated in the module file.
                print '<li class="stories_external_link">' . '<a  href="' . $storylinks['url'] . '">' . $storylinks['title'] . '</a>' . '</li>';?>
              <?php endforeach;?>
              </ul>
            </div>
          <?php endif;?>
          <?php if (!empty($content['links']['terms'])): ?>
            <div class="terms"><?php print render($content['links']['terms']); ?></div>
          <?php endif;?>
        </div>
      <?php endif;?>
    </div> <!--End of story -->
  </div> <!-- /node-inner -->
</div> <!-- /node-->
<?php print render($content['comments']); ?>
