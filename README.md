# FDSU SOTU Theme

## Node - Gulp - Sass Installation

### Installing sass and compass
- `sudo apt-get install ruby-sass ruby-compass`

### Installing nodejs
- `sudo apt-get install nodejs`
This needs to be 0.10.35 or above.

If you on Ubuntu Server, first do this:
- `sudo apt-get install python-software-properties`

Then, do this:
- `sudo add-apt-repository ppa:chris-lea/node.js`
- `sudo apt-get update`
- `sudo apt-get install nodejs`

Note: if you install *chris-lea/node.js* repo, then it installs npm version 1.4.28 (which you need to upgrade)

### Install npm
- Check if you have npm install by doing `npm -v`
- Install npm 2.1.5+
-- It has be know to work on 2.11.2
- `sudo apt-get install npm`

check version by typing 'npm -v'

- once you have npm install, to upgrade run `sudo npm install -g npm@2.11.2`
-- you may substitute 2.11.2 with any version #, or *latest*

### Install gulp command line
- `sudo npm install -g gulp`


## Gulp package setup
- inside the theme's root directory run `npm install`.  This will install all dependencies and gulp.  You should not need to run this command as sudoer

## Using Gulp
- Using gulpfile.js controls when gulp will do
- The common command to use with SASS is `gulp watch`.  This will watch your SASS folder for any changes.  When you save your scss files, gulp will compile 2 files in the css directory.  One will be a clean uncompressed stylesheet, the other one will be compressed

## Using SASS
*Do not edit the css folder.  Make all changes inside your SASS folder*

## Troubleshooting
- When setting up the gulp packages, make sure you remove the *node_modules* folder.  If you have this folder then nodejs will think it has downloaded all of the dependencies already
- Make sure you have the correct permission inside your ~/.npm folder.  This is where nodejs install all of its libraries.
- If you made a mistake and installed something with sudo when you weren't supposed to, run `sudo chown -R $(whoami) $HOME/.npm` and then `npm cache clear` before running `npm install` again.
