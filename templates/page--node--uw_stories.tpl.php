<?php

/**
 * @file
 * Template for almost the entire page. Most of the page is inside div#site.
 */

$uw_theme_branding = variable_get('uw_theme_branding', 'full');
?>
<div class="breakpoint"><div id="speed"></div></div>
<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site uw-site--sotu <?php print $content['feature_overlay_position'];?> "<?php print $attributes; ?>>
    <div class="uw-site--inner">
        <!--Section Skiplinks -->
        <div id="skip-links" class="uw-site--skip">
          <div class="uw-section--inner">
            <div id="skip" class="skip">
              <a href="#main" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
              <a href="#footer" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
            </div>
          </div>
        </div>

        <div id="header" class="uw-header--global">
            <div class="uw-section--inner">
                <?php print render($page['global_header']); ?>
            </div>
        </div>

        <div id="site--offcanvas" class="uw-site--off-canvas  <?php if (variable_get('uw_theme_branding', 'full') !== 'full'): ?>
        <?php print " generic_header";?><?php else:?><?php print " non_generic_header"; ?><?php endif;?>">
            <div class="uw-section--inner">
                <?php print render($page['site_header']); ?>
            </div>
        </div>
        <?php if ($uw_theme_branding === 'full'): ?>
            <div id="site-colors" class="uw-site--colors">
                <div class="uw-section--inner">
                    <div class="uw-site--cbar">
                        <div class="uw-site--c1 uw-cbar"></div>
                        <div class="uw-site--c2 uw-cbar"></div>
                        <div class="uw-site--c3 uw-cbar"></div>
                        <div class="uw-site--c4 uw-cbar"></div>
                    </div>
                </div>
            </div>
      <?php endif;?>
        <div id="site-header" class="uw-site--header">
            <div class="uw-section--inner">
                <a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home">
                    <?php print $site_name; ?>

                </a>
            </div>
        </div>
      <div id="uw-stories--main" class="uw-stories--main">
            <div class="uw-section--inner">
                <div id="content" class="uw_stories_sticky">

                  <div class="stories_featured_image hero ">
                    <?php if($content['use_featured_image']): ?>
                        <?php if($content['feature_image']['uri'] !== NULL): ?>
                          <?php if(isset($site_slogan) && $site_slogan !== ""): ?>
                              <span class="uw-site—subtitle"> <?php print $site_slogan; ?> </span>
                          <?php endif; ?>
                        <picture class="stories_featured_img">
                            <source srcset="<?php print $content['feature_image_style']['xlarge']; ?>" media="(min-width: 1280px)">
                          <!--    <source srcset="<//?php print $content['feature_image_style']['large']; ?>" media="(min-width: 1010)">-->
                            <source srcset="<?php print $content['feature_image_style']['large']; ?>" media="(min-width: 480px)">
                            <source srcset="<?php print $content['feature_image_style']['small']; ?>" media="(min-width: 320px)">
                            <source srcset="<?php print $content['feature_image_style']['square']; ?>">
                            <img class="stories_featured_hero_image" src="<?php print $content['feature_image_style']['large']; ?>" alt="<?php print $content['feature_image']['alt']; ?>">
                          </picture>
                        <?php endif;?>
                    <?php endif;?>
                    <?php if (isset($content['feature_image']['caption']) && $content['feature_image']['caption'] !== '' && $content['feature_image']['caption'] !== NULL): ?>
                        <input type="checkbox" id="showCaption-<?php print $node->nid; ?>" class="showCaption"/>
                        <div class="article-caption">
                          <div class="article-caption-inner">
                              <?php print $content['feature_image']['caption']; ?>
                          </div>
                        </div>
                        <label for="showCaption-<?php print $node->nid; ?>" class="sotu-show-caption">
                           <span class="small-button">Caption</span>
                        </label>
                    <?php endif; ?>
                    <div class="article-info <?php if($content['use_featured_image']): ?><?php print "use";?><?php else:?><?php print "no-use";?><?php endif;?>">
                            <span class="article-date">
                                <?php print $content['watermark_name']; ?>
                             </span>
                            <h1<?php print $title_attributes;?>><?php print $title; ?></h1>
                            <?php print $content['feature_field_subhead']; ?>
                            <span class="author">By <?php print $content['feature_author']; ?></span>
                            <span class="author-position"><?php print $content['feature_author_position']; ?></span>
                        </div>

                      <?php if (!empty($content['links'])): ?>
                          <div class="links addtoany-wrap"><?php hide($content['links']['#links']['node-readmore']); print render($content['links']); ?>

                          </div>
                      <?php endif; ?>
                    </div>

                    <?php print render($page['banner']); ?>
                </div>
            </div>
        </div>
        <div id="main" class="uw-site--main" role="main">
            <div class="uw-section--inner">
                <div id="site-navigation-wrapper" class="uw-site-navigation--wrapper">
                    <div id="site-specific-nav" class="uw-site-navigation--specific">
                        <div id="site-navigation" class="uw-site-navigation">
                            <?php if(user_access('access content')): ?>
                              <?php print render($page['sidebar_first']);?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>

                <div class="uw-site--main-top">
                    <div class="uw-site--banner"></div>

                    <div class="uw-site--messages">
                        <?php print $messages; ?>
                    </div>

                    <div class="uw-site--help">
                        <?php print render($page['help']); ?>
                    </div>

                    <!-- when logged in -->
                    <?php if ($tabs): ?>
                        <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?></div>
                    <?php endif; ?>

                    <?php if ($action_links): ?>
                        <ul class="action-links"><?php print render($action_links); ?></ul>
                    <?php endif; ?>
                </div>
                <?php $sidebar = render($page['sidebar_second']); ?>
                <?php $sidebar_promo = render($page['promo']); ?>
                <div class="uw-site-main--content page-type-uw-stories <?php  if(isset($sidebar) || isset($sidebar_promo) || isset($content['feature_field_sidebar'])) : ?><?php if(($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '') || ($content['feature_field_sidebar'] !== NULL && $content['feature_field_sidebar'] !== '')): ?><?php echo 'sidebar-present';?><?php else:?><?php echo 'no-sidebar-present';?><?php endif; ?><?php endif; ?>">
                    <div id="content" class="uw-site-content">
                        <input type="checkbox" id="article-layout-dir" class="layout-dir-check"/>
                        <?php print render($page['content']); ?>
                    </div><!--/main-content-->

                    <?php if(isset($sidebar) || isset($sidebar_promo) || isset($content['feature_field_sidebar'])) : ?>
                        <?php if(($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '') || ($content['feature_field_sidebar'] !== NULL && $content['feature_field_sidebar'] !== '')) : ?>
                            <div id="site-sidebar-wrapper" class="uw-site-sidebar--wrapper uw-sotu-sidebar">
                                <div id="site-sidebar-nav" class="uw-site-sidebar--nav">
                                    <div id="site-sidebar" class="uw-site-sidebar <?php if($sidebar_promo !== NULL && $sidebar_promo !== '') :?><?php echo ('sticky-promo');?><?php endif; ?>">
                                        <?php if(isset($sidebar_promo)) : ?>
                                            <?php if($sidebar_promo !== NULL && $sidebar_promo !== ''): ?>
                                                <div class="uw-site-sidebar--promo">
                                                    <?php print render($page['promo']); ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <div class="uw-site-sidebar--second">
                                            <?php print render($page['sidebar_second']); ?>

                                        </div>
                                        <?php if(isset($content['feature_field_sidebar'])) : ?>
                                         <?php if($content['feature_field_sidebar'] !== NULL && $content['feature_field_sidebar'] !== ''): ?>
                                            <div class="uw-site-sidebar--comp">
                                                <?php print ($content['feature_field_sidebar']); ?>
                                            </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div><!--/section inner-->
        </div><!--/site main-->
        <?php if (!drupal_is_front_page()) :?>
          <?php if (isset($content['watermark_print'])):?>
          <div id="tag" class="uw-site--tag">
              <div class="uw-section--inner">
                <div class="uw-site--tag-wrap">
                  <span class="uw-site--tag-text">

                    <?php print $content['watermark_print'];?>
                  </span>
                  <div class="facts"></div>
                </div>
              </div>
          </div>
        <?php endif; ?>
      <?php endif; ?>
      <div id="footer" class="uw-footer">
      <div id="uw-site-share" class="uw-site-share">
        <div class="uw-section--inner">
          <div class="uw-section-share"></div>
          <ul class="uw-site-share-top">
            <li class="uw-site-share--button__top">
              <a href="#main" id="uw-top-button" class="uw-top-button">
                <span class="ifdsu fdsu-arrow"></span>
                <span class="uw-footer-top-word">TOP</span>
              </a>
            </li>
            <li class="uw-site-share--button__share">
              <a href="#uw-site-share" class="uw-footer-social-button">
                <span class="ifdsu fdsu-share"></span>
                <span class="uw-footer-share-word">Share</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div id="site-footer" class="uw-site-footer site-footer-toggle open-site-footer">
        <div class="uw-section--inner">
          <div class="uw-site-footer1<?php print empty($page['site_footer']) ? ' uw-no-site-footer' : ''; ?>">
            <div class="uw-site-footer1--logo-dept">
              <?php
              $site_logo = variable_get('uw_nav_site_footer_logo');
              $site_logo_link = variable_get('uw_nav_site_footer_logo_link');
              $facebook = variable_get('uw_nav_site_footer_facebook');
              $twitter = variable_get('uw_nav_site_footer_twitter');
              $instagram = variable_get('uw_nav_site_footer_instagram');
              $youtube = variable_get('uw_nav_site_footer_youtube');
              $linkedin = variable_get('uw_nav_site_footer_linkedin');
              $snapchat = variable_get('uw_nav_site_footer_snapchat');

              $alt_tag = str_replace('_', ' ', $site_logo) . ' logo';
              $arr = explode(" ", $alt_tag);
              $arr = array_unique($arr);
              $alt_tag = implode(" ", $arr);
              ?>
              <?php if ($site_logo_link) : ?>
                <a href="<?php print $site_logo_link; ?>"><img src="<?php print base_path() . drupal_get_path('module', 'uw_nav_site_footer'); ?>/logos/<?php print $site_logo; ?>.png" alt="<?php print $alt_tag; ?>" /></a>
              <?php else : ?>
                <?php $site_name = (strtolower($site_name)); print '<a href="' . url('<front>') . '">' . (ucfirst($site_name)) . '</a>'; ?>
              <?php endif; ?>
            </div>
            <div class="uw-site-footer1--contact">
              <ul class="uw-footer-social">
                <?php if ((string) $facebook !== ''): ?>
                  <li>
                    <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>" aria-label="facebook">
                      <span class="ifdsu fdsu-facebook"></span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ((string) $twitter !== ''): ?>
                  <li>
                    <a href="https://www.twitter.com/<?php print check_plain($twitter); ?>" aria-label="twitter">
                      <span class="ifdsu fdsu-twitter"></span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ((string) $youtube !== ''): ?>
                  <li>
                    <a href="<?php print check_plain($youtube); ?>" aria-label="youtube">
                      <span class="ifdsu fdsu-youtube"></span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ((string) $instagram !== ''): ?>
                  <li>
                    <a href="<?php print check_plain($instagram); ?>" aria-label="instagram">
                      <span class="ifdsu fdsu-instagram"></span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ((string) $linkedin !== ''): ?>
                  <li>
                    <a href="<?php check_plain(print $linkedin); ?>" aria-label="linkedin">
                      <span class="ifdsu fdsu-linkedin"></span>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </div>
          </div>
          <?php if (!empty($page['site_footer']['uw_nav_site_footer_site-footer']['#node']->field_body_no_summary)): ?>
            <div class="uw-site-footer2">
              <?php print render($page['site_footer']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
      <?php print render($page['global_footer']); ?>
    </div>
  </div>
</div><!--/site-->
