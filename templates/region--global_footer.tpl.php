<?php

/**
 * @file
 * Default theme implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can
 *   be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region
 *     with underscores replaced with
 *     dashes. For example, the page_top
 *     region would have a region-page-top class.
 * - $region: The name of the region variable
 *    as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 */

$uw_theme_branding = variable_get('uw_theme_branding', 'full');
?>
<div class="uw-site-footer--global <?php print ($uw_theme_branding === 'full') ? 'non_generic_footer' : 'generic_footer'; ?>">
<div class="uw-section--inner">
<div class="uw-site-flex">
  <div id="block-uw-nav-global-footer-footer-1" class="block block-uw-nav-global-footer contextual-links-region block-odd footer-1 <?php print ($uw_theme_branding === 'full') ? 'non_generic_footer' : 'generic_footer'; ?>">
    <?php if ($uw_theme_branding === 'full') : ?>
    <div id="uw-footer-address" title="//uwaterloo.ca/" typeof="v:VCard">
      <div class="element-hidden">
        <div property="v:fn">University of Waterloo</div>
        <div data-rel="v:org">
          <div property="v:organisation-name">University of Waterloo</div>
        </div>
        <div data-rel="v:geo">
          <div property="v:latitude">43.471468</div>
          <div property="v:longitude">-80.544205</div>
        </div>

      </div>
      <div data-rel="v:adr">
        <div property="v:street-address">200 University Avenue West</div>
        <div>
          <span property="v:locality">Waterloo</span>,
          <span property="v:region">ON</span>,
          <span property="v:country-name">Canada</span>&nbsp;
          <span property="v:postal-code">N2L 3G1</span>
        </div>
      </div>
      <div class="uw-footer-phone" data-rel="v:tel">
        <a href="tel:+1-519-888-4567" property="rdf:value">+1 519 888 4567</a>
      </div>
    </div>
  </div>
  <div id="block-uw-nav-global-footer-footer-2" class="block block-uw-nav-global-footer contextual-links-region block-even footer-2">
    <ul class="global-menu">
      <li><a href="//uwaterloo.ca/about/how-find-us/contact-waterloo">Contact Waterloo</a></li>
      <li><a href="//uwaterloo.ca/map/">Maps &amp; Directions</a></li>
      <li><a href="//uwaterloo.ca/watsafe/">WatSAFE</a></li>
      <li><a href="//uwaterloo.ca/human-resources/accessibility">Accessibility</a></li>
      <li><a href="//uwaterloo.ca/privacy/">Privacy</a></li>
      <li><a href="//uwaterloo.ca/copyright">Copyright</a></li>
      <li><a href="//uwaterloo.ca/news/">Media</a></li>
      <li><a href="//uwaterloo.ca/careers/">Careers</a></li>
      <li><a href="//uwaterloo.ca/about/how-find-us/contact-waterloo/contact-form">Feedback</a></li>
    </ul>
  </div>
    <?php endif; ?>
  <div id="block-uw-nav-global-footer-footer-3" class="block block-uw-nav-global-footer contextual-links-region block-odd footer-3">
    <?php if ($uw_theme_branding === 'full') : ?>
    <div class="uw-footer-social-media">
      <ul class="uw-footer-social">
        <li><a href="https://www.facebook.com/university.waterloo" aria-label="facebook"><span class="ifdsu fdsu-facebook"></span></a></li>
        <li><a href="https://twitter.com/uWaterloo" aria-label="twitter"><span class="ifdsu fdsu-twitter"></span></a></li>
        <li><a href="https://www.youtube.com/uwaterloo" aria-label="youtube"><span class="ifdsu fdsu-youtube"></span></a></li>
        <li><a href="https://instagram.com/uofwaterloo/" aria-label="instagram"><span class="ifdsu fdsu-instagram"></span></a></li>
        <li><a href="https://www.linkedin.com/school/uwaterloo" aria-label="linkedin"><span class="ifdsu fdsu-linkedin"></span></a></li>
      </ul>
      <div class="uw-footer-social-directory"><a href="https://uwaterloo.ca/social-media/">@uwaterloo social directory</a></div>
    </div>
        <?php endif; ?>
        <?php //print $content; ?>
  </div>
</div>
</div>
</div>
